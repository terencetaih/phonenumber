package terence.demo.quiz.domain.phone;

import terence.demo.quiz.domain.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Phone {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	@Size(max = 64)
	private int phoneId;

    @NotNull
	@Size(max = 64)
	private String phoneNumber;

	@NotNull
	@Size(max = 512)
	private String description;
	
	@ManyToMany(targetEntity = User.class,cascade = CascadeType.ALL)
	@JoinTable(name = "userPhone", joinColumns = @JoinColumn(name = "phoneId", referencedColumnName = "phoneId"), inverseJoinColumns = @JoinColumn(name = "userId", referencedColumnName = "userId"))
	private Set<User> users;

    public Phone(Integer phoneId, String phoneNumber, String description) {
	    this.phoneId = phoneId;
        this.phoneNumber = phoneNumber;
        this.description = description;
    }

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
