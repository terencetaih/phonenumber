
package terence.demo.quiz.domain.phone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import terence.demo.quiz.infra.repo.PhoneRepo;
import terence.demo.quiz.infra.search.PhoneElsRepo;

import java.util.List;

@Service
public class PhoneService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneService.class);
    @Autowired
    private PhoneRepo repository;
    @Autowired
    private PhoneElsRepo elsRepository;

   
    @Transactional
    public Phone save(Phone phone) {
        LOGGER.debug("Creating {}", phone);
        return repository.save(phone);
    }

  
    @Transactional
    public List<Phone> getList() {
        LOGGER.debug("Retrieving the list of all phones");
        return repository.findAll();
    }

    public List<Phone> findByUserName(String username){
    	return elsRepository.findPhoneByUserName(username);
    }

    public List<Phone> findByUserNameDescription(String username, String description){
    	return elsRepository.findPhoneByUserAndDes(username,description);
    }
}
