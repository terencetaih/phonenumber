package terence.demo.quiz.domain.user;

import terence.demo.quiz.domain.phone.Phone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	@Size(max = 64)
	private String userId;

	@NotNull
	@Size(max = 64)
	private String userName;

	@NotNull
	@Size(max = 64)
	private String email;
	
	@ManyToMany(targetEntity=Phone.class,cascade = CascadeType.ALL)
	@JoinTable(name = "userPhone", joinColumns = @JoinColumn(name = "userId", referencedColumnName = "userId"), inverseJoinColumns = @JoinColumn(name = "phoneId", referencedColumnName = "phoneId"))
	private Set<Phone> phones;

	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}


	public User(String userId, String userName, String email) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		
	}
	

	public User(String userId, String userName, String email, Set<Phone> phones) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.phones = phones;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Phone> getPhones() {
		return phones;
	}

	public void setPhones(Set<Phone> phones) {
		this.phones = phones;
	}
	
	

}
