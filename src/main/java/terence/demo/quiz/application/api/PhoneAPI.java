package terence.demo.quiz.application.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import terence.demo.quiz.domain.phone.Phone;
import terence.demo.quiz.domain.phone.PhoneService;

import java.util.List;


@Controller
@RequestMapping("/v1/phone")
public class PhoneAPI {
    @Autowired
    private PhoneService phoneService;

    @RequestMapping
    @ResponseBody
    public String test(){
        return "OK";
    }

    @RequestMapping(value="/search/name",method=RequestMethod.GET)
    @ResponseBody
    public List<Phone> findByUser(@RequestParam(value="name") String name) {
         return phoneService.findByUserName(name);
    }

    @RequestMapping(value="/search/desc",method=RequestMethod.GET)
    @ResponseBody
    public List<Phone> findByDescription(@RequestParam(value="name") String name, @RequestParam(value="description") String description) {
         return phoneService.findByUserNameDescription(name, description);
    }

}
