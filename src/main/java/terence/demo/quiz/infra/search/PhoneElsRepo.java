package terence.demo.quiz.infra.search;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import terence.demo.quiz.domain.phone.Phone;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PhoneElsRepo implements InitializingBean {
    public final String USER_TYPE = "user";
    public final String PHONE_LIST = "phoneList";


	@Value("${elasticsearch.cluster.host}")
	private String searchingHost;
	@Value("${elasticsearch.cluster.port}")
	private int searchingPort;
	@Value("${elasticsearch.cluster.name}")
	private String clusterName;
	@Value("${elasticsearch.cluster.index}")
	private String index;

	TransportClient client;

	public void afterPropertiesSet() throws Exception {
		System.out.println("Init PhoneElsRepositoryImpl");
		Settings settings = Settings.builder()
		        .put("cluster.name", clusterName).build();
		client = new PreBuiltTransportClient(settings)
				.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(searchingHost), searchingPort));
	}


	public SearchHits findPhoneByUserNameAndDescription( String user_name,String description) {
		SearchResponse response = client.prepareSearch(index)
		        .setTypes(USER_TYPE)
		        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
		        .setQuery(QueryBuilders.matchQuery("description", description))                 // Query
		        .setPostFilter(QueryBuilders.termQuery("user.user_name",user_name))     // Filter
		        .setFrom(0).setSize(60).setExplain(true)
		        .execute()
		        .actionGet();
		return response.getHits();
	}

	public List<Phone> findPhoneByUserName(String userName) {
        GetResponse user = client.prepareGet(index, USER_TYPE, userName).setOperationThreaded(false).get();
        List<Map<String, Object>> value = (List<Map<String, Object>>) user.getSource().get(PHONE_LIST);
        List<Phone> phoneList = value.stream().map(map -> {
            return new Phone((Integer) map.get("phone_id"), (String) map.get("phone_number"), (String) map.get("description"));
        }).collect(Collectors.toList());
        return  phoneList;

    }

	public List<Phone> findPhoneByUserAndDes(String userName, String description) {
		return convertHitsToPhone(findPhoneByUserNameAndDescription(userName, description));
	}

	private List<Phone> convertHitsToPhone(SearchHits hits){
        List<Phone> ret = new ArrayList<>();
		Iterator<SearchHit> iter = hits.iterator();
		while(iter.hasNext()){
			SearchHit hit = iter.next();
			Map<String, Object> map = hit.getSource();
            Phone phone = new Phone((Integer)map.get("phone_id"), (String)map.get("phone_number"), (String)map.get("description"));
			ret.add(phone);
		}
		return ret;
	}

}
