package terence.demo.quiz.infra.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import terence.demo.quiz.domain.user.User;

@Component
public interface UserRepo extends JpaRepository<User, Integer> {
}
