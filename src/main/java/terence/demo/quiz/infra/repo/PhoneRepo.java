package terence.demo.quiz.infra.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import terence.demo.quiz.domain.phone.Phone;

@Component
public interface PhoneRepo extends JpaRepository<Phone, Integer> {

}
