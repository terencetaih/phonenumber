CREATE SCHEMA IF NOT EXISTS  `quiz` DEFAULT CHARACTER SET utf8 ;
USE `quiz`;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50)  NOT NULL,
  `email` varchar(50)  NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Table structure for table `user_phone`
--

DROP TABLE IF EXISTS `user_phone`;
CREATE TABLE `user_phone` (
  `userId` int(10) unsigned NOT NULL,
  `phone_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`userId`,`userId`),
  KEY `fk_bookpublisher_publisher_idx` (`publisher_id`),
  CONSTRAINT `user` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `phone` FOREIGN KEY (`phone_id`) REFERENCES `phone` (`phone_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `phone`;
CREATE TABLE `publisher` (
  `phone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(15) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`phone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;